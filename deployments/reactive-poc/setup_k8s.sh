#!/bin/zsh

# create resources in the necessary order
# the folder should be the expected namespace
#

NAMESPACE=`basename $PWD`

for i in `ls *.yml`; do
  kubectl create --namespace $NAMESPACE -f $i 
done
